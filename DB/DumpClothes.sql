-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: clothes_db
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `cate_id` int unsigned NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`cate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Áo'),(2,'Quần'),(3,'Đầm'),(4,'Áo Dài'),(5,'Váy');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cloth`
--

DROP TABLE IF EXISTS `cloth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cloth` (
  `cloth_id` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `cloth_name` varchar(10) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `img_1` varchar(255) NOT NULL,
  `img_2` varchar(255) NOT NULL,
  `img_3` varchar(255) NOT NULL,
  `status` int NOT NULL,
  `cate_id` int unsigned NOT NULL,
  `supp_id` int unsigned NOT NULL,
  PRIMARY KEY (`cloth_id`),
  KEY `cate_id` (`cate_id`),
  KEY `supp_id` (`supp_id`),
  KEY `status` (`status`),
  CONSTRAINT `cloth_ibfk_2` FOREIGN KEY (`cate_id`) REFERENCES `category` (`cate_id`) ON DELETE CASCADE,
  CONSTRAINT `cloth_ibfk_3` FOREIGN KEY (`status`) REFERENCES `status_list` (`status_id`),
  CONSTRAINT `cloth_ibfk_4` FOREIGN KEY (`supp_id`) REFERENCES `supplier` (`supp_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cloth`
--

LOCK TABLES `cloth` WRITE;
/*!40000 ALTER TABLE `cloth` DISABLE KEYS */;
INSERT INTO `cloth` VALUES (0001,'ĐẦM SP901','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla porttitor massa id neque aliquam vestibulum morbi. Consectetur lorem donec massa sapien faucibus et molestie ac. Mattis molestie a iaculis at erat pellentesque. Odio facilisis mauris sit amet massa vitae tortor condimentum. Blandit cursus risus at ultrices mi tempus imperdiet. Convallis tellus id interdum velit. Nisl pretium fusce id velit ut tortor pretium. Nec feugiat in fermentum posuere urna nec. Pharetra diam sit amet nisl suscipit adipiscing.','brand 12','/img/product/product-1@3x.png','','',1,3,1),(0002,'ĐẦM SP902','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla porttitor massa id neque aliquam vestibulum morbi. Consectetur lorem donec massa sapien faucibus et molestie ac. Mattis molestie a iaculis at erat pellentesque. Odio facilisis mauris sit amet massa vitae tortor condimentum. Blandit cursus risus at ultrices mi tempus imperdiet. Convallis tellus id interdum velit. Nisl pretium fusce id velit ut tortor pretium. Nec feugiat in fermentum posuere urna nec. Pharetra diam sit amet nisl suscipit adipiscing.','brand 2','/img/product/product-2@3x.png','','',2,3,2),(0003,'ĐẦM SP903','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla porttitor massa id neque aliquam vestibulum morbi. Consectetur lorem donec massa sapien faucibus et molestie ac. Mattis molestie a iaculis at erat pellentesque. Odio facilisis mauris sit amet massa vitae tortor condimentum. Blandit cursus risus at ultrices mi tempus imperdiet. Convallis tellus id interdum velit. Nisl pretium fusce id velit ut tortor pretium. Nec feugiat in fermentum posuere urna nec. Pharetra diam sit amet nisl suscipit adipiscing.','brand 3','/img/product/product-3@3x.png','','',3,3,1),(0004,'ĐẦM SP904','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla porttitor massa id neque aliquam vestibulum morbi. Consectetur lorem donec massa sapien faucibus et molestie ac. Mattis molestie a iaculis at erat pellentesque. Odio facilisis mauris sit amet massa vitae tortor condimentum. Blandit cursus risus at ultrices mi tempus imperdiet. Convallis tellus id interdum velit. Nisl pretium fusce id velit ut tortor pretium. Nec feugiat in fermentum posuere urna nec. Pharetra diam sit amet nisl suscipit adipiscing.','brand 4','/img/product/product-4@3x.png','','',3,3,2),(0005,'ĐẦM SP905','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla porttitor massa id neque aliquam vestibulum morbi. Consectetur lorem donec massa sapien faucibus et molestie ac. Mattis molestie a iaculis at erat pellentesque. Odio facilisis mauris sit amet massa vitae tortor condimentum. Blandit cursus risus at ultrices mi tempus imperdiet. Convallis tellus id interdum velit. Nisl pretium fusce id velit ut tortor pretium. Nec feugiat in fermentum posuere urna nec. Pharetra diam sit amet nisl suscipit adipiscing.','brand 5','/img/product/product-5@3x.png','','',3,3,1),(0006,'ĐẦM SP906','Bạn đã có chiếc đầm hở vai làm mưa làm gió trong mùa hè thu năm nay chưa. Hãy sắm ngay một chiếc từ thương hiệu De Leah với 2 mẫu màu đen tuyền và hông thạch anh siêu hot này nhé.','brand 2','/img/product/product-6@3x.png','','',2,3,2),(0007,'ĐẦM SP907','Bạn đã có chiếc đầm hở vai làm mưa làm gió trong mùa hè thu năm nay chưa. Hãy sắm ngay một chiếc từ thương hiệu De Leah với 2 mẫu màu đen tuyền và hông thạch anh siêu hot này nhé.','brand 2','/img/product/product-7@3x.png','','',3,3,2),(0008,'ĐẦM SP908','Bạn đã có chiếc đầm hở vai làm mưa làm gió trong mùa hè thu năm nay chưa. Hãy sắm ngay một chiếc từ thương hiệu De Leah với 2 mẫu màu đen tuyền và hông thạch anh siêu hot này nhé.','brand 5','/img/product/product-8@3x.png','','',3,3,3),(0009,'ĐẦM SP909','Bạn đã có chiếc đầm hở vai làm mưa làm gió trong mùa hè thu năm nay chưa. Hãy sắm ngay một chiếc từ thương hiệu De Leah với 2 mẫu màu đen tuyền và hông thạch anh siêu hot này nhé.','brand 1','/img/product/product-9@3x.png','/img/model/md-chietietsp/sub-img-1.png','/img/model/md-chietietsp/sub-img-2.png',3,3,1),(0010,'ĐẦM SP910','Bạn đã có chiếc đầm hở vai làm mưa làm gió trong mùa hè thu năm nay chưa. Hãy sắm ngay một chiếc từ thương hiệu De Leah với 2 mẫu màu đen tuyền và hông thạch anh siêu hot này nhé.','brand 5','/img/product/product-10@3x.png','','',3,3,3),(0011,'ĐẦM SP911','Bạn đã có chiếc đầm hở vai làm mưa làm gió trong mùa hè thu năm nay chưa. Hãy sắm ngay một chiếc từ thương hiệu De Leah với 2 mẫu màu đen tuyền và hông thạch anh siêu hot này nhé.','brand 3','/img/product/product-11@3x.png','','',2,3,3),(0012,'ĐẦM SP912','Bạn đã có chiếc đầm hở vai làm mưa làm gió trong mùa hè thu năm nay chưa. Hãy sắm ngay một chiếc từ thương hiệu De Leah với 2 mẫu màu đen tuyền và hông thạch anh siêu hot này nhé.','brand 3','/img/product/product-12@3x.png','','',3,3,1);
/*!40000 ALTER TABLE `cloth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `colors` (
  `color_id` int unsigned NOT NULL AUTO_INCREMENT,
  `color` varchar(255) NOT NULL,
  PRIMARY KEY (`color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'đỏ'),(2,'cam'),(3,'đen'),(4,'trắng');
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_detail` (
  `od_id` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `quantity` int unsigned NOT NULL,
  `variant_id` int unsigned NOT NULL,
  PRIMARY KEY (`od_id`),
  KEY `variant_id` (`variant_id`),
  CONSTRAINT `order_detail_ibfk_1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`variant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `total_cost` int NOT NULL,
  `order_time` date NOT NULL,
  `od_id` int unsigned NOT NULL,
  `user_id` int unsigned NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `od_id` (`od_id`),
  KEY `cus_id` (`user_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`od_id`) REFERENCES `order_detail` (`od_id`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sizes`
--

DROP TABLE IF EXISTS `sizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sizes` (
  `size_id` int unsigned NOT NULL AUTO_INCREMENT,
  `size` varchar(255) NOT NULL,
  PRIMARY KEY (`size_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sizes`
--

LOCK TABLES `sizes` WRITE;
/*!40000 ALTER TABLE `sizes` DISABLE KEYS */;
INSERT INTO `sizes` VALUES (1,'xs'),(2,'s'),(3,'m'),(4,'l'),(5,'xl'),(7,'xxl');
/*!40000 ALTER TABLE `sizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_list`
--

DROP TABLE IF EXISTS `status_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_list` (
  `status_id` int NOT NULL AUTO_INCREMENT,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_list`
--

LOCK TABLES `status_list` WRITE;
/*!40000 ALTER TABLE `status_list` DISABLE KEYS */;
INSERT INTO `status_list` VALUES (1,'Sale'),(2,'Out of order'),(3,'Normal');
/*!40000 ALTER TABLE `status_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supplier` (
  `supp_id` int unsigned NOT NULL AUTO_INCREMENT,
  `supp_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `supp_phone` int NOT NULL,
  `supp_address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`supp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES (1,'Supplier 1',123456789,'Supplier 1\'s address'),(2,'Supplier 2',987654321,'Supplier 2\'s address'),(3,'Supplier 3',147258369,'Supplier 3\'s address'),(4,'Supplier 4',96385271,'Supplier 4\'s address');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `user_id` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` int unsigned NOT NULL,
  `address` varchar(255) NOT NULL,
  `role` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0001,'vi.vinh0312@gmail.com','123456','vinh','0',113,'1jhjhjhjhjhhjhj',1),(0002,'dungdo1165@gmail.com','duydung','Duy Dung','0',113,'111111111111',0),(0008,'vinh123','123456','vince','0',123456,'0123456',0),(0009,'123','123','123','0',123,'123',0),(0021,'abcxyz@mail.com','adminabc123','Dung','Do',1214547845,'ODNEEE',1),(0031,'abcxyz@gmail.com','12345678','','',0,'some where',1),(0034,'admin@gmail.com','adminabc12','Affort','Ben',1214547842,'home',0),(0035,'admin123@mail.com','adminabc123','Dung','Do',121454784,'home',0),(0036,'admin@mail.com','adminabc123','Dung','Do',121454784,'home',0),(0037,'ngggiahuy@gmail.com','Abcd1234','Gia','Nguyen',858017549,'home',0),(0038,'admin@bmail.com','adminabc12','Dung','Do',121454784,'home',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `variant`
--

DROP TABLE IF EXISTS `variant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant` (
  `variant_id` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `variant_stock` int unsigned NOT NULL,
  `price` int unsigned NOT NULL,
  `sold` int unsigned NOT NULL,
  `cloth_id` int(4) unsigned zerofill NOT NULL,
  `size_id` int unsigned NOT NULL,
  `color_id` int unsigned NOT NULL,
  PRIMARY KEY (`variant_id`),
  UNIQUE KEY `variant_id` (`variant_id`),
  KEY `cloth_id` (`cloth_id`),
  KEY `size_id` (`size_id`),
  KEY `color_id` (`color_id`),
  CONSTRAINT `variant_ibfk_1` FOREIGN KEY (`cloth_id`) REFERENCES `cloth` (`cloth_id`) ON DELETE CASCADE,
  CONSTRAINT `variant_ibfk_2` FOREIGN KEY (`color_id`) REFERENCES `colors` (`color_id`),
  CONSTRAINT `variant_ibfk_3` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`size_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variant`
--

LOCK TABLES `variant` WRITE;
/*!40000 ALTER TABLE `variant` DISABLE KEYS */;
INSERT INTO `variant` VALUES (0001,9,69000,0,0003,2,2),(0002,2,96000,0,0002,1,1),(0003,9,50000,0,0001,1,3),(0004,9,23000,3,0001,1,4),(0005,10,1000000,0,0001,3,1),(0006,10,30000,0,0001,1,2),(0007,10,30000,0,0001,5,1),(0008,10,30000,0,0002,1,1),(0009,10,20000,0,0002,2,1),(0010,10,90000,0,0002,3,1),(0011,10,45000,0,0002,5,1),(0012,10,60900,0,0003,1,2),(0013,10,30000,0,0003,3,2),(0014,10,30000,0,0003,4,1),(0015,10,30000,0,0003,5,2),(0016,10,30000,0,0004,1,1),(0017,10,30000,0,0004,2,2),(0018,10,30000,0,0004,3,1),(0019,10,30000,0,0004,4,2),(0020,10,30000,0,0004,5,2),(0021,10,30000,0,0005,1,1),(0022,10,30000,0,0005,2,2),(0023,10,30000,0,0005,3,2),(0024,10,30000,0,0005,4,1),(0025,10,30000,0,0005,5,2),(0028,2,69000,20,0001,1,4),(0031,1,96000,0,0001,1,1);
/*!40000 ALTER TABLE `variant` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-22 20:21:29
